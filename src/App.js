// import logo from './logo.svg';
import { useState } from 'react';
import './App.css';

function App() {
  const [numVal, setNumVal] = useState("")
  const [flagNum, setFlagNum] = useState("isPrime")
  const roundNum = (data) => {
    if (parseInt(data) > 4) {
      return parseInt(numVal.replace(".", "")) + 1
    }
    else {
      return parseInt(numVal.replace(".", ""))
    }
  }

  const onChangeNum = (e) => {
    var value = e.target.value
    var addVal = e.nativeEvent.data
    var regNumWithDot = /^\d+$|\.|-/g;
    const isNumWithDot = regNumWithDot.test(value)
    // const isNum = regNum.test(value)
    const isNeg = parseInt(value) < 0
    const lastIsDot = numVal[numVal.length - 1] === "."
    console.log(value)
    console.log("numVal.length", numVal)
    if (numVal.length == 0 && value == "-") {
      return setNumVal(value)
    }
    if (numVal.length == 1 && value == "") {
      return setNumVal("")
    }
    if (isNumWithDot) {
      if (isNeg) {
        console.log("isNeg")
        return setNumVal("1")
      }
      if (lastIsDot) {
        console.log("lastIsDot")
        const roundNumber = (lastIsDot && addVal !== ".") ? roundNum(addVal) : numVal
        return setNumVal(roundNumber)
      }
      else {
        if (numVal.length == 1 && numVal == 0) {
          return setNumVal(parseInt(value).toString())
        }
        else {
          console.log("basic")
          return setNumVal(value)
        }
      }
    }


  }
  const isPrime = () => {
    if (numVal.length == 0) return false;
    var num = parseInt(numVal)
    if (num <= 3) return num > 1;

    if ((num % 2 === 0) || (num % 3 === 0)) return false;

    let count = 5;

    while (Math.pow(count, 2) <= num) {
      if (num % count === 0 || num % (count + 2) === 0) return false;

      count += 6;
    }

    return true;
  }
  var isFib = () => {
    if (numVal.length == 0) return false;
    var n = parseInt(numVal)
    var a = 0;
    var b = 1;
    if (n == a || n == b) return true;
    var c = a + b;
    while (c <= n) {
      if (c == n) return true;
      a = b;
      b = c;
      c = a + b;
    }
    return false;
  };

  const calAlgo = () => {
    if (flagNum == "isPrime") {
      console.log("isPrime()", isPrime())
      return isPrime()
    }
    else {
      return isFib()
    }
  }

  return (
    <div className="App">
      <div className="leftCol">
        <input type="text" value={numVal} onChange={(e) => onChangeNum(e)} />
      </div>
      <div className="middleCol">
        <select onChange={(e) => setFlagNum(e.target.value)} value={flagNum}>
          <option value="isPrime">isPrime</option>
          <option value="isFibonacci">isFibonacci</option>
        </select>
      </div>
      <div className="rightCol">
        <text>{calAlgo().toString()}</text>
      </div>
    </div>
  );
}

export default App;
